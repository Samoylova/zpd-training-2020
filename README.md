# ZPD-training-2020

Services for A/D CTF training from 1155 with love :heart:.

# :bomb: tavern

It is an online-bar where you can drink in quarantine days.

![preview](./preview.png)

### Tags
- nodejs
- python
- mongodb
- docker

### Vulnerabilities
- RCE in the `hash` function. Fix: sanitize user input.
- Weak hashing algorithm: the hash algorithm depends on a single letter in the password. Fix: change hash function. [Sploit.](./sploits/tavern/brute.py)
- User authentication is missed in the websocket functionality. `get_recipe` method doesn't check user authentication. Fix: add cookie validation in websocket functionality. [Sploit.](./sploits/tavern/websock.py)
- Usage of vulnerable version of `jsonwebtoken` module. It allows [crafting arbitrary jwt tokens](https://auth0.com/blog/critical-vulnerabilities-in-json-web-token-libraries/) by changing the signature algorithm from `HS256` to `None` method. Fix: increase `jsonwebtoken` module's version. [Sploit.](./sploits/tavern/jwt.py)

### Deploy

#### Service

```
cd ./services/tavern
docker-compose up -d
```

#### Checker

TODO: Fix it to check user list is available.

Install dependencies:
```
pip3 install websocket-client
```

# :bomb: baby_notes

It is a notes storage.

### Tags
- binary
- docker

### Vulnerabilities
- RCE in `registr`, `get_notes` functions. [Sploit.](./sploits/baby_train/rce_sploit.py)
- Buffer overflow in `store_note` function. [Sploit.](./sploits/baby_train/buffer_overflow_sploit.py)

Fixes:

- Rewrite service in python or in another language.
- Patch `UsefulString` with another string. It makes sploit developing more difficult.
- Use LD_PRELOAD for filtering input for `system` function. But it only protects you from RCE.

### Deploy

#### Service

```
cd ./services/baby_train
docker-compose up -d
```

## Contributors
### Service developers
- @nicki_skit: (`tavern`'s frontend)
- @msh_smlv: (`tavern`'s backend, `baby_notes`)

### Exsploit developers
- @natalyakisa (exploits for the `tavern` service)
- @msh_smlv (exploits for the `baby_notes` service)

### Inspiration
- @manmoleculo
